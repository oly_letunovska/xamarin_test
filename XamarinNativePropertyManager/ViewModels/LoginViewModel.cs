/*
 *  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 *  See LICENSE in the source repository root for complete license information.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json.Linq;
using XamarinNativePropertyManager.Models;
using XamarinNativePropertyManager.Services;

namespace XamarinNativePropertyManager.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IGraphService _graphService;
        private readonly IConfigService _configService;

        public ICommand LoginCommand => new MvxAsyncCommand(LoginAsync);

        public LoginViewModel(IGraphService graphService, IConfigService configService)
        {
            _graphService = graphService;
            _configService = configService;
        }

        private async Task LoginAsync()
        {
            // Navigate to the groups view.
            ShowViewModel<GroupsViewModel>();
        }
    }
}
