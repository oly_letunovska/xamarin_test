// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace XamarinNativePropertyManager.iOS.Views
{
    [Register ("DetailsView")]
    partial class DetailsView
    {
        [Outlet]
        UIKit.UIView ContentView { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint DescriptionLabelTopConstraint { get; set; }


        [Outlet]
        UIKit.UITextField DescriptionTextField { get; set; }


        [Outlet]
        UIKit.UITextView DescriptionTextView { get; set; }


        [Outlet]
        UIKit.UITextField LivingAreaTextField { get; set; }


        [Outlet]
        UIKit.UITextField LotSizeTextField { get; set; }


        [Outlet]
        UIKit.UITextField OperatingCostsTextField { get; set; }


        [Outlet]
        UIKit.UITextField RoomsTextField { get; set; }


        [Outlet]
        UIKit.UIScrollView ScrollView { get; set; }


        [Outlet]
        UIKit.UILabel StreetNameLabel { get; set; }


        [Outlet]
        UIKit.UITextField StreetNameTextField { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DescriptionLabelTopConstraint != null) {
                DescriptionLabelTopConstraint.Dispose ();
                DescriptionLabelTopConstraint = null;
            }

            if (DescriptionTextField != null) {
                DescriptionTextField.Dispose ();
                DescriptionTextField = null;
            }

            if (DescriptionTextView != null) {
                DescriptionTextView.Dispose ();
                DescriptionTextView = null;
            }

            if (LivingAreaTextField != null) {
                LivingAreaTextField.Dispose ();
                LivingAreaTextField = null;
            }

            if (LotSizeTextField != null) {
                LotSizeTextField.Dispose ();
                LotSizeTextField = null;
            }

            if (OperatingCostsTextField != null) {
                OperatingCostsTextField.Dispose ();
                OperatingCostsTextField = null;
            }

            if (RoomsTextField != null) {
                RoomsTextField.Dispose ();
                RoomsTextField = null;
            }

            if (StreetNameLabel != null) {
                StreetNameLabel.Dispose ();
                StreetNameLabel = null;
            }

            if (StreetNameTextField != null) {
                StreetNameTextField.Dispose ();
                StreetNameTextField = null;
            }
        }
    }
}