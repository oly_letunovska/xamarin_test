// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace XamarinNativePropertyManager.iOS.Views.Cells
{
    [Register ("ConversationsTableRightViewCell")]
    partial class ConversationsTableRightViewCell
    {
        [Outlet]
        UIKit.UILabel MessageLabel { get; set; }


        [Outlet]
        UIKit.UILabel SenderLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (MessageLabel != null) {
                MessageLabel.Dispose ();
                MessageLabel = null;
            }

            if (SenderLabel != null) {
                SenderLabel.Dispose ();
                SenderLabel = null;
            }
        }
    }
}