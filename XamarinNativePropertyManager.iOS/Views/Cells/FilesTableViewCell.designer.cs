// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace XamarinNativePropertyManager.iOS.Views.Cells
{
    [Register ("FilesTableViewCell")]
    partial class FilesTableViewCell
    {
        [Outlet]
        UIKit.UIImageView ImageView { get; set; }


        [Outlet]
        UIKit.UILabel NameLabel { get; set; }


        [Outlet]
        UIKit.UILabel UrlLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ImageView != null) {
                ImageView.Dispose ();
                ImageView = null;
            }

            if (NameLabel != null) {
                NameLabel.Dispose ();
                NameLabel = null;
            }

            if (UrlLabel != null) {
                UrlLabel.Dispose ();
                UrlLabel = null;
            }
        }
    }
}