﻿/*
 *  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 *  See LICENSE in the source repository root for complete license information.
 */

using MvvmCross.iOS.Views;
using XamarinNativePropertyManager.ViewModels;
using MvvmCross.Binding.BindingContext;
using XamarinNativePropertyManager.iOS.Extensions;
using UIKit;

namespace XamarinNativePropertyManager.iOS.Views
{
	public partial class LoginView : MvxViewController<LoginViewModel>
	{
		public LoginView() : base("LoginView", null)
		{
			
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// Set navigation bar style.
			this.SetNavigationBarStyle();

			// Hide the navigation bar.
			this.HideNavigationBar();

            LoginText.ResignFirstResponder();
            PasswordText.ResignFirstResponder();
			// Create and apply the binding set.
			var set = this.CreateBindingSet<LoginView, LoginViewModel>();
			set.Bind(SignInButton).To(vm => vm.LoginCommand);
			set.Bind(SignInButton).For("Visibility").To(vm => vm.IsLoading).WithConversion("InvertedVisibility");
			set.Bind(ActivityIndicator).For("Visibility").To(vm => vm.IsLoading).WithConversion("Visibility");
			set.Apply();

            var viewTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);
            });
            viewTap.CancelsTouchesInView = false;
            View.AddGestureRecognizer(viewTap);
            PasswordText.EditingDidEndOnExit += (sender, args) =>
            {
                PasswordText.ResignFirstResponder();
            };
		}
        //View appear
		public override void ViewWillAppear(bool animated)
		{
			// Hide the navigation bar.
			this.HideNavigationBar(true);
			ViewModel.OnResume();
			base.ViewWillAppear(animated);
		}
	}
}


