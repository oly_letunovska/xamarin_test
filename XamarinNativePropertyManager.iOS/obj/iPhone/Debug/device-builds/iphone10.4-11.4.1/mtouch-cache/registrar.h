#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
#define DEBUG 1
#include <stdarg.h>
#include <objc/objc.h>
#include <objc/runtime.h>
#include <objc/message.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h>
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreGraphics/CoreGraphics.h>

@class UIApplicationDelegate;
@class UITableViewSource;
@class UIKit_UIControlEventProxy;
@class UICollectionViewSource;
@class UIImagePickerControllerDelegate;
@class UIPickerViewModel;
@class Foundation_InternalNSNotificationHandler;
@class Foundation_NSDispatcher;
@class __MonoMac_NSActionDispatcher;
@class __MonoMac_NSSynchronizationContextDispatcher;
@class Foundation_NSAsyncDispatcher;
@class __MonoMac_NSAsyncActionDispatcher;
@class __MonoMac_NSAsyncSynchronizationContextDispatcher;
@class MvvmCross_iOS_Platform_MvxApplicationDelegate;
@class AppDelegate;
@class MvvmCross_Platform_iOS_Views_MvxEventSourceTabBarController;
@class MvvmCross_iOS_Views_MvxTabBarViewController;
@class MvvmCross_iOS_Views_MvxTabBarViewController_1;
@class GroupView;
@class MvvmCross_Binding_iOS_Views_MvxBaseTableViewSource;
@class MvvmCross_Binding_iOS_Views_MvxTableViewSource;
@class XamarinNativePropertyManager_iOS_Views_Tabs_ConversationsTabViewSource;
@class MvvmCross_Platform_iOS_Views_MvxEventSourceViewController;
@class MvvmCross_iOS_Views_MvxViewController;
@class MvvmCross_iOS_Views_MvxViewController_1;
@class DetailsView;
@class GroupsView;
@class LoginView;
@class ConversationsTabView;
@class DetailsTabView;
@class FilesTabView;
@class TasksTabView;
@class MvvmCross_Binding_iOS_Views_MvxTableViewCell;
@class ConversationsTableLeftViewCell;
@class ConversationsTableRightViewCell;
@class FilesTableViewCell;
@class GroupsTableViewCell;
@class TasksTableViewCell;
@class UIKit_UIBarButtonItem_Callback;
@class __UIGestureRecognizerToken;
@class __UIGestureRecognizerParameterlessToken;
@class __UITapGestureRecognizer;
@class __UISwipeGestureRecognizer;
@class UIKit_UIImagePickerController__UIImagePickerControllerDelegate;
@class UIKit_UIView_UIViewAppearance;
@class UIKit_UINavigationBar_UINavigationBarAppearance;
@class UIKit_UIPageViewController__UIPageViewControllerDataSource;
@class UIKit_UISearchBar__UISearchBarDelegate;
@class UIKit_UITextField__UITextFieldDelegate;
@class UIKit_UIScrollView__UIScrollViewDelegate;
@class UIKit_UITextView__UITextViewDelegate;
@class __NSObject_Disposer;
@class MvvmCross_Platform_iOS_Views_MvxEventSourceTableViewController;
@class MvvmCross_iOS_Views_MvxTableViewController;
@class MvvmCross_iOS_Views_MvxTableViewController_1;
@class MvvmCross_Platform_iOS_Views_MvxEventSourceCollectionViewController;
@class MvvmCross_iOS_Views_MvxCollectionViewController;
@class MvvmCross_iOS_Views_MvxCollectionViewController_1;
@class MvvmCross_Platform_iOS_Views_MvxEventSourcePageViewController;
@class MvvmCross_iOS_Views_MvxPageViewController;
@class MvvmCross_iOS_Views_MvxPageViewController_1;
@class MvvmCross_iOS_Views_MvxUIRefreshControl;
@class MvvmCross_Binding_iOS_Views_MvxCollectionReusableView;
@class MvxImageView;
@class MvvmCross_Binding_iOS_Views_MvxPickerViewModel;
@class MvvmCross_Binding_iOS_Views_MvxBaseCollectionViewSource;
@class MvvmCross_Binding_iOS_Views_MvxStandardTableViewCell;
@class MvvmCross_Binding_iOS_Views_MvxCollectionViewCell;
@class MvvmCross_Binding_iOS_Views_MvxSimpleTableViewSource;
@class MvvmCross_Binding_iOS_Views_MvxStandardTableViewSource;
@class MvvmCross_Binding_iOS_Views_MvxView;
@class MvvmCross_Binding_iOS_Views_MvxCollectionViewSource;
@class MvvmCross_Binding_iOS_Views_MvxCollectionViewSourceAnimated;
@class MvvmCross_Binding_iOS_Views_MvxActionBasedTableViewSource;
@class Microsoft_Identity_Client_WebUI;

@interface UIApplicationDelegate : NSObject<UIApplicationDelegate> {
}
	-(id) init;
@end

@interface UITableViewSource : NSObject<UIScrollViewDelegate> {
}
	-(id) init;
@end

@interface UICollectionViewSource : NSObject<UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegate> {
}
	-(id) init;
@end

@interface UIImagePickerControllerDelegate : NSObject<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UINavigationControllerDelegate> {
}
	-(id) init;
@end

@interface UIPickerViewModel : NSObject<UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(id) init;
@end

@interface MvvmCross_iOS_Platform_MvxApplicationDelegate : NSObject<UIApplicationDelegate> {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) applicationWillEnterForeground:(UIApplication *)p0;
	-(void) applicationDidEnterBackground:(UIApplication *)p0;
	-(void) applicationWillTerminate:(UIApplication *)p0;
	-(void) applicationDidFinishLaunching:(UIApplication *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface AppDelegate : MvvmCross_iOS_Platform_MvxApplicationDelegate<UIApplicationDelegate> {
}
	-(UIWindow *) window;
	-(void) setWindow:(UIWindow *)p0;
	-(BOOL) application:(UIApplication *)p0 didFinishLaunchingWithOptions:(NSDictionary *)p1;
	-(BOOL) application:(UIApplication *)p0 openURL:(NSURL *)p1 options:(NSDictionary *)p2;
	-(void) applicationWillResignActive:(UIApplication *)p0;
	-(void) applicationDidEnterBackground:(UIApplication *)p0;
	-(void) applicationWillEnterForeground:(UIApplication *)p0;
	-(void) applicationDidBecomeActive:(UIApplication *)p0;
	-(void) applicationWillTerminate:(UIApplication *)p0;
	-(id) init;
@end

@interface MvvmCross_Platform_iOS_Views_MvxEventSourceTabBarController : UITabBarController {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) viewWillDisappear:(BOOL)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLoad;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface MvvmCross_iOS_Views_MvxTabBarViewController : MvvmCross_Platform_iOS_Views_MvxEventSourceTabBarController {
}
	-(id) init;
@end

@interface MvvmCross_iOS_Views_MvxTabBarViewController_1 : MvvmCross_iOS_Views_MvxTabBarViewController {
}
	-(id) init;
@end

@interface GroupView : MvvmCross_iOS_Views_MvxTabBarViewController_1 {
}
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidLoad;
	-(id) init;
@end

@interface MvvmCross_Binding_iOS_Views_MvxBaseTableViewSource : NSObject<UIScrollViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) tableView:(UITableView *)p0 accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)p1;
	-(void) tableView:(UITableView *)p0 didSelectRowAtIndexPath:(NSIndexPath *)p1;
	-(UITableViewCell *) tableView:(UITableView *)p0 cellForRowAtIndexPath:(NSIndexPath *)p1;
	-(void) tableView:(UITableView *)p0 didEndDisplayingCell:(UITableViewCell *)p1 forRowAtIndexPath:(NSIndexPath *)p2;
	-(NSInteger) numberOfSectionsInTableView:(UITableView *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_Binding_iOS_Views_MvxTableViewSource : MvvmCross_Binding_iOS_Views_MvxBaseTableViewSource<UIScrollViewDelegate> {
}
	-(NSInteger) tableView:(UITableView *)p0 numberOfRowsInSection:(NSInteger)p1;
@end

@interface XamarinNativePropertyManager_iOS_Views_Tabs_ConversationsTabViewSource : MvvmCross_Binding_iOS_Views_MvxTableViewSource<UIScrollViewDelegate> {
}
	-(CGFloat) tableView:(UITableView *)p0 heightForRowAtIndexPath:(NSIndexPath *)p1;
@end

@interface MvvmCross_Platform_iOS_Views_MvxEventSourceViewController : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) viewWillDisappear:(BOOL)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLoad;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface MvvmCross_iOS_Views_MvxViewController : MvvmCross_Platform_iOS_Views_MvxEventSourceViewController {
}
	-(id) init;
@end

@interface MvvmCross_iOS_Views_MvxViewController_1 : MvvmCross_iOS_Views_MvxViewController {
}
	-(id) init;
@end

@interface DetailsView : MvvmCross_iOS_Views_MvxViewController_1 {
}
	@property (nonatomic, assign) UIView * ContentView;
	@property (nonatomic, assign) NSLayoutConstraint * DescriptionLabelTopConstraint;
	@property (nonatomic, assign) UITextField * DescriptionTextField;
	@property (nonatomic, assign) UITextView * DescriptionTextView;
	@property (nonatomic, assign) UITextField * LivingAreaTextField;
	@property (nonatomic, assign) UITextField * LotSizeTextField;
	@property (nonatomic, assign) UITextField * OperatingCostsTextField;
	@property (nonatomic, assign) UITextField * RoomsTextField;
	@property (nonatomic, assign) UIScrollView * ScrollView;
	@property (nonatomic, assign) UILabel * StreetNameLabel;
	@property (nonatomic, assign) UITextField * StreetNameTextField;
	-(UIView *) ContentView;
	-(void) setContentView:(UIView *)p0;
	-(NSLayoutConstraint *) DescriptionLabelTopConstraint;
	-(void) setDescriptionLabelTopConstraint:(NSLayoutConstraint *)p0;
	-(UITextField *) DescriptionTextField;
	-(void) setDescriptionTextField:(UITextField *)p0;
	-(UITextView *) DescriptionTextView;
	-(void) setDescriptionTextView:(UITextView *)p0;
	-(UITextField *) LivingAreaTextField;
	-(void) setLivingAreaTextField:(UITextField *)p0;
	-(UITextField *) LotSizeTextField;
	-(void) setLotSizeTextField:(UITextField *)p0;
	-(UITextField *) OperatingCostsTextField;
	-(void) setOperatingCostsTextField:(UITextField *)p0;
	-(UITextField *) RoomsTextField;
	-(void) setRoomsTextField:(UITextField *)p0;
	-(UIScrollView *) ScrollView;
	-(void) setScrollView:(UIScrollView *)p0;
	-(UILabel *) StreetNameLabel;
	-(void) setStreetNameLabel:(UILabel *)p0;
	-(UITextField *) StreetNameTextField;
	-(void) setStreetNameTextField:(UITextField *)p0;
	-(void) viewDidLoad;
	-(void) viewWillAppear:(BOOL)p0;
	-(id) init;
@end

@interface GroupsView : MvvmCross_iOS_Views_MvxViewController_1 {
}
	@property (nonatomic, assign) UISearchBar * SearchBar;
	@property (nonatomic, assign) UITableView * TableView;
	-(UISearchBar *) SearchBar;
	-(void) setSearchBar:(UISearchBar *)p0;
	-(UITableView *) TableView;
	-(void) setTableView:(UITableView *)p0;
	-(void) viewDidLoad;
	-(void) viewWillAppear:(BOOL)p0;
	-(id) init;
@end

@interface LoginView : MvvmCross_iOS_Views_MvxViewController_1 {
}
	@property (nonatomic, assign) UIActivityIndicatorView * ActivityIndicator;
	@property (nonatomic, assign) UIButton * SignInButton;
	@property (nonatomic, assign) UITextField * LoginText;
	@property (nonatomic, assign) UITextField * PasswordText;
	-(UIActivityIndicatorView *) ActivityIndicator;
	-(void) setActivityIndicator:(UIActivityIndicatorView *)p0;
	-(UIButton *) SignInButton;
	-(void) setSignInButton:(UIButton *)p0;
	-(UITextField *) LoginText;
	-(void) setLoginText:(UITextField *)p0;
	-(UITextField *) PasswordText;
	-(void) setPasswordText:(UITextField *)p0;
	-(void) viewDidLoad;
	-(void) viewWillAppear:(BOOL)p0;
	-(id) init;
@end

@interface ConversationsTabView : MvvmCross_iOS_Views_MvxViewController {
}
	@property (nonatomic, assign) UITableView * TableView;
	-(UITableView *) TableView;
	-(void) setTableView:(UITableView *)p0;
	-(void) viewDidLoad;
	-(id) init;
@end

@interface DetailsTabView : MvvmCross_iOS_Views_MvxViewController {
}
	@property (nonatomic, assign) UIView * ContentView;
	@property (nonatomic, assign) UILabel * DescriptionLabel;
	@property (nonatomic, assign) UILabel * LivingAreaLabel;
	@property (nonatomic, assign) UILabel * LotSizeLabel;
	@property (nonatomic, assign) UILabel * OperatingCostsLabel;
	@property (nonatomic, assign) UILabel * RoomsLabel;
	@property (nonatomic, assign) UIScrollView * ScrollView;
	-(UIView *) ContentView;
	-(void) setContentView:(UIView *)p0;
	-(UILabel *) DescriptionLabel;
	-(void) setDescriptionLabel:(UILabel *)p0;
	-(UILabel *) LivingAreaLabel;
	-(void) setLivingAreaLabel:(UILabel *)p0;
	-(UILabel *) LotSizeLabel;
	-(void) setLotSizeLabel:(UILabel *)p0;
	-(UILabel *) OperatingCostsLabel;
	-(void) setOperatingCostsLabel:(UILabel *)p0;
	-(UILabel *) RoomsLabel;
	-(void) setRoomsLabel:(UILabel *)p0;
	-(UIScrollView *) ScrollView;
	-(void) setScrollView:(UIScrollView *)p0;
	-(void) viewDidLoad;
	-(void) viewDidLayoutSubviews;
	-(id) init;
@end

@interface FilesTabView : MvvmCross_iOS_Views_MvxViewController {
}
	@property (nonatomic, assign) UITableView * TableView;
	-(UITableView *) TableView;
	-(void) setTableView:(UITableView *)p0;
	-(void) viewDidLoad;
	-(id) init;
@end

@interface TasksTabView : MvvmCross_iOS_Views_MvxViewController {
}
	@property (nonatomic, assign) UITableView * TableView;
	-(UITableView *) TableView;
	-(void) setTableView:(UITableView *)p0;
	-(void) viewDidLoad;
	-(id) init;
@end

@interface MvvmCross_Binding_iOS_Views_MvxTableViewCell : UITableViewCell {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface ConversationsTableLeftViewCell : MvvmCross_Binding_iOS_Views_MvxTableViewCell {
}
	@property (nonatomic, assign) UILabel * MessageLabel;
	@property (nonatomic, assign) UILabel * SenderLabel;
	-(UILabel *) MessageLabel;
	-(void) setMessageLabel:(UILabel *)p0;
	-(UILabel *) SenderLabel;
	-(void) setSenderLabel:(UILabel *)p0;
@end

@interface ConversationsTableRightViewCell : MvvmCross_Binding_iOS_Views_MvxTableViewCell {
}
	@property (nonatomic, assign) UILabel * MessageLabel;
	@property (nonatomic, assign) UILabel * SenderLabel;
	-(UILabel *) MessageLabel;
	-(void) setMessageLabel:(UILabel *)p0;
	-(UILabel *) SenderLabel;
	-(void) setSenderLabel:(UILabel *)p0;
@end

@interface FilesTableViewCell : MvvmCross_Binding_iOS_Views_MvxTableViewCell {
}
	@property (nonatomic, assign) UIImageView * ImageView;
	@property (nonatomic, assign) UILabel * NameLabel;
	@property (nonatomic, assign) UILabel * UrlLabel;
	-(UIImageView *) ImageView;
	-(void) setImageView:(UIImageView *)p0;
	-(UILabel *) NameLabel;
	-(void) setNameLabel:(UILabel *)p0;
	-(UILabel *) UrlLabel;
	-(void) setUrlLabel:(UILabel *)p0;
@end

@interface GroupsTableViewCell : MvvmCross_Binding_iOS_Views_MvxTableViewCell {
}
	@property (nonatomic, assign) UILabel * MailLabel;
	@property (nonatomic, assign) UILabel * NameLabel;
	-(UILabel *) MailLabel;
	-(void) setMailLabel:(UILabel *)p0;
	-(UILabel *) NameLabel;
	-(void) setNameLabel:(UILabel *)p0;
@end

@interface TasksTableViewCell : MvvmCross_Binding_iOS_Views_MvxTableViewCell {
}
	@property (nonatomic, assign) UILabel * TitleLabel;
	-(UILabel *) TitleLabel;
	-(void) setTitleLabel:(UILabel *)p0;
@end

@interface __UIGestureRecognizerToken : NSObject {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface __UIGestureRecognizerParameterlessToken : __UIGestureRecognizerToken {
}
	-(void) target;
@end

@interface UIKit_UIView_UIViewAppearance : NSObject {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface UIKit_UINavigationBar_UINavigationBarAppearance : UIKit_UIView_UIViewAppearance {
}
	-(void) setTitleTextAttributes:(NSDictionary *)p0;
@end

@interface MvvmCross_Platform_iOS_Views_MvxEventSourceTableViewController : UITableViewController {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) viewWillDisappear:(BOOL)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLoad;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_iOS_Views_MvxTableViewController : MvvmCross_Platform_iOS_Views_MvxEventSourceTableViewController {
}
@end

@interface MvvmCross_iOS_Views_MvxTableViewController_1 : MvvmCross_iOS_Views_MvxTableViewController {
}
@end

@interface MvvmCross_Platform_iOS_Views_MvxEventSourceCollectionViewController : UICollectionViewController {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) viewWillDisappear:(BOOL)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLoad;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_iOS_Views_MvxCollectionViewController : MvvmCross_Platform_iOS_Views_MvxEventSourceCollectionViewController {
}
@end

@interface MvvmCross_iOS_Views_MvxCollectionViewController_1 : MvvmCross_iOS_Views_MvxCollectionViewController {
}
@end

@interface MvvmCross_Platform_iOS_Views_MvxEventSourcePageViewController : UIPageViewController {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) viewDidLoad;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewWillDisappear:(BOOL)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_iOS_Views_MvxPageViewController : MvvmCross_Platform_iOS_Views_MvxEventSourcePageViewController {
}
	-(void) viewDidLoad;
@end

@interface MvvmCross_iOS_Views_MvxPageViewController_1 : MvvmCross_iOS_Views_MvxPageViewController {
}
@end

@interface MvvmCross_iOS_Views_MvxUIRefreshControl : UIRefreshControl {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface MvvmCross_Binding_iOS_Views_MvxCollectionReusableView : UICollectionReusableView {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface MvxImageView : UIImageView {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_Binding_iOS_Views_MvxPickerViewModel : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)p0;
	-(NSInteger) pickerView:(UIPickerView *)p0 numberOfRowsInComponent:(NSInteger)p1;
	-(NSString *) pickerView:(UIPickerView *)p0 titleForRow:(NSInteger)p1 forComponent:(NSInteger)p2;
	-(void) pickerView:(UIPickerView *)p0 didSelectRow:(NSInteger)p1 inComponent:(NSInteger)p2;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_Binding_iOS_Views_MvxBaseCollectionViewSource : NSObject<UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) collectionView:(UICollectionView *)p0 didSelectItemAtIndexPath:(NSIndexPath *)p1;
	-(UICollectionViewCell *) collectionView:(UICollectionView *)p0 cellForItemAtIndexPath:(NSIndexPath *)p1;
	-(void) collectionView:(UICollectionView *)p0 didEndDisplayingCell:(UICollectionViewCell *)p1 forItemAtIndexPath:(NSIndexPath *)p2;
	-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_Binding_iOS_Views_MvxStandardTableViewCell : MvvmCross_Binding_iOS_Views_MvxTableViewCell {
}
	-(void) setSelected:(BOOL)p0 animated:(BOOL)p1;
@end

@interface MvvmCross_Binding_iOS_Views_MvxCollectionViewCell : UICollectionViewCell {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(UICollectionViewLayoutAttributes *) preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface MvvmCross_Binding_iOS_Views_MvxSimpleTableViewSource : MvvmCross_Binding_iOS_Views_MvxTableViewSource<UIScrollViewDelegate> {
}
@end

@interface MvvmCross_Binding_iOS_Views_MvxStandardTableViewSource : MvvmCross_Binding_iOS_Views_MvxTableViewSource<UIScrollViewDelegate> {
}
@end

@interface MvvmCross_Binding_iOS_Views_MvxView : UIView {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface MvvmCross_Binding_iOS_Views_MvxCollectionViewSource : MvvmCross_Binding_iOS_Views_MvxBaseCollectionViewSource<UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegate> {
}
	-(NSInteger) collectionView:(UICollectionView *)p0 numberOfItemsInSection:(NSInteger)p1;
@end

@interface MvvmCross_Binding_iOS_Views_MvxCollectionViewSourceAnimated : MvvmCross_Binding_iOS_Views_MvxCollectionViewSource<UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegate> {
}
	-(NSInteger) collectionView:(UICollectionView *)p0 numberOfItemsInSection:(NSInteger)p1;
@end

@interface MvvmCross_Binding_iOS_Views_MvxActionBasedTableViewSource : MvvmCross_Binding_iOS_Views_MvxStandardTableViewSource<UIScrollViewDelegate> {
}
@end


